package com.view.centralApp.departments;

import javax.swing.*;

public class CreateDepartmentFrame extends JFrame {
    private JTextField departmentName;
    private JComboBox<Object> managerBox;
    private JButton createDepartmentButton;
    private JPanel globalPane;

    public CreateDepartmentFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JTextField getDepartmentName() {
        return departmentName;
    }

    public JComboBox<Object> getManagerBox() {
        return managerBox;
    }

    public JButton getCreateDepartmentButton() {
        return createDepartmentButton;
    }
}
