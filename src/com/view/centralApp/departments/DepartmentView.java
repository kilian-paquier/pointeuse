package com.view.centralApp.departments;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class DepartmentView extends JFrame {
    private JPanel globalPanel;
    private JTable employeesTable;
    private JLabel departmentName;
    private JButton seeManagerButton;
    private JLabel managerLabel;

    public DepartmentView(String name) {
        super(name);
        setContentPane(globalPanel);
    }

    public void setEmployeesTable(DefaultTableModel employeesTable) {
        this.employeesTable.setModel(employeesTable);
    }

    public JLabel getDepartmentName() {
        return departmentName;
    }

    public JButton getSeeManagerButton() {
        return seeManagerButton;
    }

    public JPanel getGlobalPanel() {
        return globalPanel;
    }

    public JTable getEmployeesTable() {
        return employeesTable;
    }

    public JLabel getManagerLabel() {
        return managerLabel;
    }
}
