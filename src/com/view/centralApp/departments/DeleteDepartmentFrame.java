package com.view.centralApp.departments;

import javax.swing.*;

public class DeleteDepartmentFrame extends JFrame {
    private JComboBox<Object> departmentBox;
    private JButton deleteDepartmentButton;
    private JPanel globalPane;

    public DeleteDepartmentFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JComboBox<Object> getDepartmentBox() {
        return departmentBox;
    }

    public JButton getDeleteDepartmentButton() {
        return deleteDepartmentButton;
    }
}
