package com.view.centralApp.departments;

import javax.swing.*;

public class ModifyDepartmentFrame extends JFrame {
    private JComboBox<Object> departmentBox;
    private JTextField departmentName;
    private JButton modifyDepartmentButton;
    private JPanel globalPane;

    public ModifyDepartmentFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JComboBox<Object> getDepartmentBox() {
        return departmentBox;
    }

    public JTextField getDepartmentName() {
        return departmentName;
    }

    public JButton getModifyDepartmentButton() {
        return modifyDepartmentButton;
    }
}
