package com.view.centralApp;

import com.controller.CentralAppController;
import com.view.centralApp.checks.ImportCheckFrame;
import com.view.centralApp.departments.CreateDepartmentFrame;
import com.view.centralApp.departments.DeleteDepartmentFrame;
import com.view.centralApp.departments.ModifyDepartmentFrame;
import com.view.centralApp.employees.CreateEmployeeFrame;
import com.view.centralApp.employees.DeleteEmployeeFrame;
import com.view.centralApp.employees.ModifyEmployeeFrame;
import com.view.centralApp.imports.ExportEmployeesFrame;
import com.view.centralApp.imports.ImportEmployeesFrame;
import com.view.centralApp.settings.IncidenceFrame;
import com.view.centralApp.settings.IpAndPortFrame;

import javax.swing.*;
import java.io.File;
import java.io.PrintWriter;

public class CentralApp extends JFrame {
    private JMenuBar menuBar;
    private JMenu settings;
    private JMenuItem ipAndPort;
    private JMenuItem incidence;
    private JMenu checks;
    private JMenuItem addCheck;
    private JMenuItem importCheck;
    private JMenu imports;
    private JMenuItem importEmployees;
    private JMenuItem exportEmployees;
    private JMenu employees;
    private JMenuItem createEmployee;
    private JMenuItem modifyEmployee;
    private JMenuItem deleteEmployee;
    private JMenu departments;
    private JMenuItem createDepartment;
    private JMenuItem modifyDepartment;
    private JMenuItem deleteDepartment;

    private JTable employeesTable;
    private JTable departmentsTable;
    private JTable checksTable;
    private JTable employeesEvent;
    private JTable departmentEvent;
    private JButton updateButton;
    private JComboBox<Object> dateBox;
    private JComboBox<Object> departmentBox;
    private JButton filtrateButton;
    private JPanel globalPane;

    private IpAndPortFrame ipAndPortFrame;
    private IncidenceFrame incidenceFrame;
    private ImportCheckFrame importCheckFrame;
    private ExportEmployeesFrame exportEmployeesFrame;
    private ImportEmployeesFrame importEmployeesFrame;
    private CreateEmployeeFrame createEmployeeFrame;
    private ModifyEmployeeFrame modifyEmployeeFrame;
    private DeleteEmployeeFrame deleteEmployeeFrame;
    private CreateDepartmentFrame createDepartmentFrame;
    private ModifyDepartmentFrame modifyDepartmentFrame;
    private DeleteDepartmentFrame deleteDepartmentFrame;

    public CentralApp(String name) {
        super(name);
        initComponents();
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        if (new File(".\\data\\mainAppServer.lock").exists()) {
            System.out.println("Already a main app opened");
        }
        else {
            CentralAppController controller = new CentralAppController("Main app");
            controller.getCentralApp().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            controller.getCentralApp().pack();
            controller.getCentralApp().setSize(700, 600);
            controller.getCentralApp().setLocationRelativeTo(null);
            controller.getCentralApp().setVisible(true);
        }
    }

    private void initComponents() {
        menuBar = new JMenuBar();
        settings = new JMenu();
        ipAndPort = new JMenuItem();
        incidence = new JMenuItem();
        checks = new JMenu();
        addCheck = new JMenuItem();
        importCheck = new JMenuItem();
        imports = new JMenu();
        importEmployees = new JMenuItem();
        exportEmployees = new JMenuItem();
        employees = new JMenu();
        createEmployee = new JMenuItem();
        modifyEmployee = new JMenuItem();
        deleteEmployee = new JMenuItem();
        departments = new JMenu();
        createDepartment = new JMenuItem();
        modifyDepartment = new JMenuItem();
        deleteDepartment = new JMenuItem();

        ipAndPortFrame = new IpAndPortFrame("Change Settings");
        incidenceFrame = new IncidenceFrame("Incidence");
        importCheckFrame = new ImportCheckFrame("Import Checks");
        exportEmployeesFrame = new ExportEmployeesFrame("Export Employees");
        importEmployeesFrame = new ImportEmployeesFrame("Import Employees");
        createEmployeeFrame = new CreateEmployeeFrame("Create Employee");
        modifyEmployeeFrame = new ModifyEmployeeFrame("Modify Employee");
        deleteEmployeeFrame = new DeleteEmployeeFrame("Delete Employee");
        createDepartmentFrame = new CreateDepartmentFrame("Create Department");
        modifyDepartmentFrame = new ModifyDepartmentFrame("Modify Department");
        deleteDepartmentFrame = new DeleteDepartmentFrame("Delete Department");

        // Setting the JMenu Settings
        settings.setText("Settings");
        ipAndPort.setText("IP and Port");
        settings.add(ipAndPort);
        incidence.setText("Incidence");
        settings.add(incidence);
        menuBar.add(settings);

        // Setting the JMenu Checks
        checks.setText("Checks");
        addCheck.setText("Add Check");
        checks.add(addCheck);
        importCheck.setText("Import Checks");
        checks.add(importCheck);
        menuBar.add(checks);

        // Setting the JMenu Imports
        imports.setText("Imports");
        importEmployees.setText("Import Employees");
        imports.add(importEmployees);
        exportEmployees.setText("Export Employees");
        imports.add(exportEmployees);
        menuBar.add(imports);

        // Setting the JMenu Employees
        employees.setText("Employees");
        createEmployee.setText("Create Employee");
        employees.add(createEmployee);
        modifyEmployee.setText("Modify Employee");
        employees.add(modifyEmployee);
        deleteEmployee.setText("Delete Employee");
        employees.add(deleteEmployee);
        menuBar.add(employees);

        // Setting the JMenu Departments
        departments.setText("Departments");
        createDepartment.setText("Create Department");
        departments.add(createDepartment);
        modifyDepartment.setText("Modify Department");
        departments.add(modifyDepartment);
        deleteDepartment.setText("Delete Department");
        departments.add(deleteDepartment);
        menuBar.add(departments);

        setJMenuBar(menuBar);
        setContentPane(globalPane);
    }

    public JMenu getSettings() {
        return settings;
    }

    public JMenuItem getIpAndPort() {
        return ipAndPort;
    }

    public JMenuItem getIncidence() {
        return incidence;
    }

    public JMenu getChecks() {
        return checks;
    }

    public JMenuItem getAddCheck() {
        return addCheck;
    }

    public JMenuItem getImportCheck() {
        return importCheck;
    }

    public JMenu getImports() {
        return imports;
    }

    public JMenuItem getImportEmployees() {
        return importEmployees;
    }

    public JMenuItem getExportEmployees() {
        return exportEmployees;
    }

    public JMenu getEmployees() {
        return employees;
    }

    public JMenuItem getCreateEmployee() {
        return createEmployee;
    }

    public JMenuItem getModifyEmployee() {
        return modifyEmployee;
    }

    public JMenuItem getDeleteEmployee() {
        return deleteEmployee;
    }

    public JMenu getDepartments() {
        return departments;
    }

    public JMenuItem getCreateDepartment() {
        return createDepartment;
    }

    public JMenuItem getModifyDepartment() {
        return modifyDepartment;
    }

    public JMenuItem getDeleteDepartment() {
        return deleteDepartment;
    }

    public JTable getEmployeesTable() {
        return employeesTable;
    }

    public JTable getDepartmentsTable() {
        return departmentsTable;
    }

    public JTable getChecksTable() {
        return checksTable;
    }

    public JTable getEmployeesEvent() {
        return employeesEvent;
    }

    public JTable getDepartmentEvent() {
        return departmentEvent;
    }

    public JButton getUpdateButton() {
        return updateButton;
    }

    public JComboBox<Object> getDateBox() {
        return dateBox;
    }

    public JComboBox<Object> getDepartmentBox() {
        return departmentBox;
    }

    public JButton getFiltrateButton() {
        return filtrateButton;
    }

    public JPanel getGlobalPane() {
        return globalPane;
    }

    public IpAndPortFrame getIpAndPortFrame() {
        return ipAndPortFrame;
    }

    public IncidenceFrame getIncidenceFrame() {
        return incidenceFrame;
    }

    public ImportCheckFrame getImportCheckFrame() {
        return importCheckFrame;
    }

    public ExportEmployeesFrame getExportEmployeesFrame() {
        return exportEmployeesFrame;
    }

    public ImportEmployeesFrame getImportEmployeesFrame() {
        return importEmployeesFrame;
    }

    public CreateEmployeeFrame getCreateEmployeeFrame() {
        return createEmployeeFrame;
    }

    public ModifyEmployeeFrame getModifyEmployeeFrame() {
        return modifyEmployeeFrame;
    }

    public DeleteEmployeeFrame getDeleteEmployeeFrame() {
        return deleteEmployeeFrame;
    }

    public CreateDepartmentFrame getCreateDepartmentFrame() {
        return createDepartmentFrame;
    }

    public ModifyDepartmentFrame getModifyDepartmentFrame() {
        return modifyDepartmentFrame;
    }

    public DeleteDepartmentFrame getDeleteDepartmentFrame() {
        return deleteDepartmentFrame;
    }
}
