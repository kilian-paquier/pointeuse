package com.view.centralApp.settings;

import javax.swing.*;

public class IpAndPortFrame extends JFrame {
    private JTextField ipField;
    private JTextField portField;
    private JButton changeSettingsButton;
    private JPanel globalPane;

    public IpAndPortFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JTextField getIpField() {
        return ipField;
    }

    public JTextField getPortField() {
        return portField;
    }

    public JButton getChangeSettingsButton() {
        return changeSettingsButton;
    }

}
