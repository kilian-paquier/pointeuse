package com.view.centralApp.settings;

import javax.swing.*;

public class IncidenceFrame extends JFrame {
    private JComboBox<Object> incidenceBox;
    private JButton changeIncidenceButton;
    private JPanel globalPane;

    public IncidenceFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JComboBox<Object> getIncidenceBox() {
        return incidenceBox;
    }

    public JButton getChangeIncidenceButton() {
        return changeIncidenceButton;
    }
}
