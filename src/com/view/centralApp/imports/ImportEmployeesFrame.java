package com.view.centralApp.imports;

import javax.swing.*;

public class ImportEmployeesFrame extends JFrame {
    private JTextField filePath;
    private JButton importEmployeesButton;
    private JPanel globalPane;

    public ImportEmployeesFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JTextField getFilePath() {
        return filePath;
    }

    public JButton getImportEmployeesButton() {
        return importEmployeesButton;
    }
}
