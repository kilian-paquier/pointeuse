package com.view.centralApp.imports;

import javax.swing.*;

public class ExportEmployeesFrame extends JFrame {
    private JTextField filePath;
    private JButton exportEmployeesButton;
    private JPanel globalPane;

    public ExportEmployeesFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JTextField getFilePath() {
        return filePath;
    }

    public JButton getExportEmployeesButton() {
        return exportEmployeesButton;
    }
}
