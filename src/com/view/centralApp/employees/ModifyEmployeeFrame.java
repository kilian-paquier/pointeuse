package com.view.centralApp.employees;

import javax.swing.*;

public class ModifyEmployeeFrame extends JFrame {
    private JComboBox<Object> employeeBox;
    private JTextField firstName;
    private JTextField lastName;
    private JTextField mail;
    private JComboBox<Object> departmentBox;
    private JComboBox<Object> dayBox;
    private JComboBox<Object> morningHour;
    private JComboBox<Object> morningMinute;
    private JComboBox<Object> afternoonMinute;
    private JComboBox<Object> afternoonHour;
    private JButton modifyEmployeeButton;
    private JPanel globalPane;

    public ModifyEmployeeFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JComboBox<Object> getEmployeeBox() {
        return employeeBox;
    }

    public JTextField getFirstName() {
        return firstName;
    }

    public JTextField getLastName() {
        return lastName;
    }

    public JTextField getMail() {
        return mail;
    }

    public JComboBox<Object> getDepartmentBox() {
        return departmentBox;
    }

    public JComboBox<Object> getDayBox() {
        return dayBox;
    }

    public JComboBox<Object> getMorningHour() {
        return morningHour;
    }

    public JComboBox<Object> getMorningMinute() {
        return morningMinute;
    }

    public JComboBox<Object> getAfternoonMinute() {
        return afternoonMinute;
    }

    public JComboBox<Object> getAfternoonHour() {
        return afternoonHour;
    }

    public JButton getModifyEmployeeButton() {
        return modifyEmployeeButton;
    }
}
