package com.view.centralApp.employees;

import javax.swing.*;

public class DeleteEmployeeFrame extends JFrame {
    private JComboBox<Object> employeeBox;
    private JButton deleteEmployeeButton;
    private JPanel globalPane;

    public DeleteEmployeeFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JComboBox<Object> getEmployeeBox() {
        return employeeBox;
    }

    public JButton getDeleteEmployeeButton() {
        return deleteEmployeeButton;
    }
}
