package com.view.centralApp.employees;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class EmployeeView extends JFrame {
    private JLabel firstName;
    private JLabel lastName;
    private JLabel mail;
    private JLabel departmentName;
    private JTable tableChecks;
    private JTabbedPane globalPane;
    private JLabel countHour;
    private JTable hoursTable;

    public EmployeeView(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JTabbedPane getGlobalPane() {
        return globalPane;
    }

    public JLabel getDepartmentName() {
        return departmentName;
    }

    public JLabel getMail() {
        return mail;
    }

    public JLabel getLastName() {
        return lastName;
    }

    public JLabel getFirstName() {
        return firstName;
    }

    public JLabel getCountHour() {
        return countHour;
    }

    public JTable getTableChecks() {
        return tableChecks;
    }

    public JTable getHoursTable() {
        return hoursTable;
    }

    public void setHoursTable(DefaultTableModel hoursTable) {
        this.hoursTable.setModel(hoursTable);
    }

    public void setTableChecks(DefaultTableModel checkTable) {
        this.tableChecks.setModel(checkTable);
    }
}
