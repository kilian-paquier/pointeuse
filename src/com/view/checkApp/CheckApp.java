package com.view.checkApp;

import com.controller.CheckAppController;

import javax.swing.*;
import java.io.File;

public class CheckApp extends JFrame{
    private JPanel globalPanelCheck;
    private JButton checkInOutButton;
    private JComboBox<Object> employeesBox;
    private JLabel dateLabel;
    private JLabel theoryHourLabel;
    private JLabel realHourLabel;
    private JButton syncFromMainAppButton;

    public CheckApp(String name) {
        super(name);
        setContentPane(globalPanelCheck);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        if (new File(".\\data\\serverTracker.lock").exists()) {
            System.out.println("Already a time tracker opened");
        }
        else {
            CheckAppController checkAppController = new CheckAppController("Time tracker");
            checkAppController.getCheckApp().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            checkAppController.getCheckApp().pack();
            checkAppController.getCheckApp().setSize(450, 300);
            checkAppController.getCheckApp().setResizable(false);
            checkAppController.getCheckApp().setLocation(200, 200);
            checkAppController.getCheckApp().setVisible(true);
        }
    }

    public JPanel getGlobalPanelCheck() {
        return globalPanelCheck;
    }

    public JButton getCheckInOutButton() {
        return checkInOutButton;
    }

    public JComboBox<Object> getEmployeesBox() {
        return employeesBox;
    }

    public JLabel getDateLabel() {
        return dateLabel;
    }

    public JLabel getTheoryHourLabel() {
        return theoryHourLabel;
    }

    public JLabel getRealHourLabel() {
        return realHourLabel;
    }

    public JButton getSyncFromMainAppButton() {
        return syncFromMainAppButton;
    }
}
