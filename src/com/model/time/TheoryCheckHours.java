package com.model.time;

import java.time.LocalTime;

/**
 * This class correspond to hours in one day for an employee
 */
public class TheoryCheckHours {
    // Attributes
    private LocalTime inHour;
    private LocalTime outHour;

    // Constructors
    public TheoryCheckHours(){

    }

    /**
     * The constructor taking in arguments the hours and the minutes of when the employee should check on morning and afternoon, for one day
     * @param morningHour the hour of morning
     * @param morningMinute the minute of morning
     * @param afternoonHour the hour of afternoon
     * @param afternoonMinute the minute of afternoon
     */
    public TheoryCheckHours(int morningHour, int morningMinute, int afternoonHour, int afternoonMinute){
        inHour = LocalTime.of(morningHour, morningMinute,0,0);
        outHour = LocalTime.of(afternoonHour,afternoonMinute,0,0);
        roundTimeIn();
        roundTimeOut();
    }

    /**
     * The constructor taking in argument two localtime which correspond to the time the employee should check on morning and afternoon
     * @param morningTime The localtime of the morning
     * @param afternoonTime The localtime of the afternoon
     */
    public TheoryCheckHours(LocalTime morningTime, LocalTime afternoonTime){
        inHour = LocalTime.of(morningTime.getHour(), morningTime.getMinute(),0,0);
        outHour = LocalTime.of(afternoonTime.getHour(), afternoonTime.getMinute(),0,0);
        roundTimeIn();
        roundTimeOut();
    }

    // Methods
    public LocalTime getInHour() {
        return inHour;
    }

    public LocalTime getOutHour() {
        return outHour;
    }

    public void setInHour(LocalTime inHour) {
        this.inHour = inHour;
        roundTimeIn();
    }

    public void setOutHour(LocalTime outHour) {
        this.outHour = outHour;
        roundTimeOut();
    }

    public void printHourMorning(){
        System.out.println("Must begin at : "+getInHour());
    }

    public void printHourAfternoon(){
        System.out.println("Must leave at : "+getOutHour());
    }

    /**
     * This function round the time the employee should check on morning using the function round(minute, hour)
     */
    public void roundTimeIn() {
        int minute = inHour.getMinute();
        int hour = inHour.getHour();
        int[] timeResult = round(minute,hour);
        inHour = inHour.withHour(timeResult[1]);
        inHour = inHour.withMinute(timeResult[0]);
    }

    /**
     * This function round the time the employee should check on afternoon using the function round(minute, hour)
     */
    public void roundTimeOut() {
        int minute = outHour.getMinute();
        int hour = outHour.getHour();
        int[] timeResult = round(minute,hour);
        outHour = outHour.withHour(timeResult[1]);
        outHour = outHour.withMinute(timeResult[0]);
    }

    /**
     * This function round the minute and increment the hour if the minute is round from 53 to 0
     * @param minute the minute
     * @param hour the hour
     * @return An array which correspond to the minute and the hour rounded
     */
    private int[] round(int minute, int hour){
        if (minute <= 7)
            minute = 0;
        else if (minute <= 22)
            minute = 15;
        else if (minute <= 37)
            minute = 30;
        else if (minute <= 52)
            minute = 45;
        else {
            minute = 0;
            hour++;
        }
        return new int[]{minute, hour};
    }
}
