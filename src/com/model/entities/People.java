package com.model.entities;

/**
 * An abstract class which determine a person
 */
public abstract class People {
    // Attributes
    private String firstName;
    private String lastName;
    private String email;

    // Constructors
    /**
     * Constructor taking a first name, a last name and an email
     * @param firstName the first name
     * @param lastName the last name
     * @param email the mail
     */
    public People(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    // Methods
    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return getFirstName()+','+getLastName()+','+getEmail();
    }
}
