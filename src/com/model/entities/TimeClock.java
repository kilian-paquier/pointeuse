package com.model.entities;

import com.google.gson.Gson;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Pointeuse
 */
public class TimeClock {
    private LocalTime timeOfChecking;
    private LocalDate date;
    private int idEmployee;
    private String firstName;
    private String lastName;


    /**
     * Creating a checking time of a special employee
     * \Warning Only need to bu used in the application
     *
     * @param id        the id of the employee
     * @param firstName the first name of the employee
     * @param lastName  the last name of the employee
     */
    public TimeClock(int id, String firstName, String lastName) {
        timeOfChecking = LocalTime.now().withNano(0).withSecond(0);
        date = LocalDate.now();
        this.firstName = firstName;
        this.lastName = lastName;
        this.idEmployee = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getId() {
        return idEmployee;
    }

    /**
     * This function is used to save the checking time of an employee if the time clock can't communicate with the mainClass application
     *
     * @throws IOException Throw if there's an error in writing in the file
     */
    public void saveToJson() throws IOException {
        Gson gson = new Gson();
        String obj = gson.toJson(this);
        FileWriter writer2 = new FileWriter(".\\TimeClockTempFiles\\employeesCheckings.json", true);
        writer2.write(obj);
        writer2.write("\n");
        writer2.close();
    }

    /**
     * This function is used to read the checking time of an employee if the time clock couldn't communicate with the mainClass app before
     *
     * @return a list of TimeClock
     * @throws IOException Thrown if the file is not found
     */
    public static List<TimeClock> readFromJson() throws IOException {
        Gson gson = new Gson();
        TimeClock timeClock;
        List<TimeClock> timeClockList = new ArrayList<>();
        File file = new File(".\\TimeClockTempFiles\\employeesCheckings.json");
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);

        String line;
        while ((line = reader.readLine()) != null) {
            timeClock = gson.fromJson(line, TimeClock.class);
            timeClockList.add(timeClock);
        }
        reader.close();
        fileReader.close();
        boolean isDelete = file.delete();
        return timeClockList;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return firstName + "," + lastName + "," + timeOfChecking.toString() + "," + date.toString();
    }
}
