package com.model.entities;

import com.model.time.TheoryCheckHours;
import com.model.time.WorkToday;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * This class correspond to one employee
 */
public class Employee extends People {
    // Attributes
    private static int nbEmployeesID = 0;
    private int idEmployee;
    private int countHour = 0;
    private List<WorkToday> workDaysCheck = new ArrayList<>();
    private Map<DayOfWeek, TheoryCheckHours> checkHours = new HashMap<>(5);
    private int idDepartment;

    // Constructors

    /**
     * Constructor taking an id, a first name, a last name and an email
     * @param idEmployee the id of the employee
     * @param firstName the first name
     * @param lastName the last name
     * @param email the mail
     */
    public Employee(int idEmployee, String firstName, String lastName, String email) {
        super(firstName, lastName, email);
        nbEmployeesID++;
        idDepartment = -1;
        this.idEmployee = idEmployee;
    }

    /**
     * Constructor taking a first name, a last name and an email
     * @param firstName the first name
     * @param lastName the last name
     * @param email the mail
     */
    public Employee(String firstName, String lastName, String email) {
        super(firstName, lastName, email);
        nbEmployeesID++;
        idDepartment = -1;
        this.idEmployee = nbEmployeesID;
    }

    /**
     * Constructor taking an id, a first name, a last name
     * @param idEmployee the id of the employee
     * @param firstName the first name
     * @param lastName the last name
     */
    public Employee(int idEmployee, String firstName, String lastName) {
        super(firstName, lastName, null);
        nbEmployeesID++;
        idDepartment = -1;
        this.idEmployee = idEmployee;
    }

    /**
     * Constructor taking a first name, a last name
     * @param firstName the first name
     * @param lastName the last name
     */
    public Employee(String firstName, String lastName) {
        super(firstName, lastName, null);
        nbEmployeesID++;
        idDepartment = -1;
        this.idEmployee = nbEmployeesID;
    }

    // Methods

    public int getIdDepartment() {
        return idDepartment;
    }

    public void addCountHour(int lateTime){
        this.countHour += lateTime;
    }

    public int getIdEmployee() {
        return idEmployee;
    }

    public int getCountHour() {
        return countHour;
    }

    public TheoryCheckHours getCheckHoursDay(DayOfWeek day){
        return checkHours.get(day);
    }

    public Map<DayOfWeek, TheoryCheckHours> getCheckHours() {
        return checkHours;
    }

    /**
     * This function create a check of today with a LocalTime of this morning
     * @param realHourMorning The hour he checked on morning
     * @param date the date of checking
     */
    public void setWorkDaysCheck(LocalTime realHourMorning, LocalDate date) {
        if (realHourMorning == null)
            workDaysCheck.add(new WorkToday());
        else
            workDaysCheck.add(new WorkToday(realHourMorning));
        workDaysCheck.get(workDaysCheck.size() - 1).setDate(date);
    }

    /**
     * This function add the hour the employee checked on afternoon
     * @param realHourAfternoon the LocalTime of this afternoon
     */
    public void addAfternoonTimeWorkToday(LocalTime realHourAfternoon){
        workDaysCheck.get(workDaysCheck.size()-1).setRealHourAfternoon(realHourAfternoon);
    }

    /**
     * This function permit to set the hours of beginning and ending for a special day
     * @param day the day
     * @param morningTime the hour he has to begin
     * @param afternoonTime the hour he has to end
     */
    public void setTheoryCheckHours(DayOfWeek day, LocalTime morningTime, LocalTime afternoonTime){
        TheoryCheckHours check = new TheoryCheckHours(morningTime, afternoonTime);
        checkHours.put(day, check);
    }

    /**
     * This function search the workToday corresponding to the date passed in argument
     * @param date the date of the workToday we want
     * @return the workToday if it exists
     */
    public WorkToday getDay(LocalDate date) {
        for (WorkToday today : workDaysCheck) {
            if (today.getDate().equals(date))
                return today;
        }
        return null;
    }

    /**
     * This function permits to compare the time of checking this morning and the time he should have checked, permits also to add to count hour his late time
     * @param date the date we want to compare to
     * @return the late time value
     */
    public int compareTimeMorning(LocalDate date) {
        TheoryCheckHours hoursOfToday = getCheckHoursDay(date.getDayOfWeek()); // Getting the theories times the employee should check today
        LocalTime realCheckTime = getDay(date).getRealHourMorning(); // Getting the time he really checked
        int lateHours = hoursOfToday.getInHour().getHour() - realCheckTime.getHour(); // Counting if he has more than an hour late
        int lateMinutes = hoursOfToday.getInHour().getMinute() - realCheckTime.getMinute(); // Counting if he has more than 15 minutes late
        int lateTime = lateHours * 60 + lateMinutes; // Init late time
        return (-lateTime);
    }

    /**
     * This function compare the time of cheking this afternoon and the time he should have checked, permits also to add to count hour his late time
     * @param date the date we want to compare to
     * @return the late time value
     */
    public int compareTimeAfternoon(LocalDate date) {
        TheoryCheckHours hoursOfToday = getCheckHoursDay(date.getDayOfWeek()); // Getting the theories times the employee should check today
        LocalTime realCheckTime = getDay(date).getRealHourAfternoon(); // Getting the time he really checked
        int lateHours = hoursOfToday.getOutHour().getHour() - realCheckTime.getHour(); // Counting if he has more than an hour late
        int lateMinutes = hoursOfToday.getOutHour().getMinute() - realCheckTime.getMinute(); // Counting if he has more than 15 minutes late
        return lateHours * 60 + lateMinutes;
    }

    public List<WorkToday> getWorkDaysCheck() {
        return workDaysCheck;
    }

    @Override
    public String toString() {
        return super.toString()+','+getIdEmployee()+','+getCountHour()+','+getIdDepartment();
    }

    public void setIdDepartment(int idDepartment) {
        this.idDepartment = idDepartment;
    }

    public void setWorkDaysCheck(List<WorkToday> workDaysCheck) {
        this.workDaysCheck = workDaysCheck;
    }

    public void setCheckHours(Map<DayOfWeek, TheoryCheckHours> checkHours) {
        this.checkHours = checkHours;
    }
}