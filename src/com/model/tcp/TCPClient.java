package com.model.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TCPClient extends TCPMessage implements Runnable {
    private String information;
    private List<String> employees = new ArrayList<>();
    private String ip;
    private int port;
    private boolean hasCommunicate = true;


    public TCPClient(String ip, int port) {
        s = null;
        isA = null;
        this.ip = ip;
        this.port = port;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    /**
     * Setting the socket
     * @throws IOException if the server is offline
     */
    private void setSocket() throws IOException {
        setStreamBuffer(size);
        isA = new InetSocketAddress(ip, port);
        s = new Socket(isA.getHostName(), isA.getPort());
        setStreamBuffer(s.getReceiveBufferSize());
        s.setSoTimeout(200000);
    }

    /**
     * This function is the "main" function of a client, it sends the check point or a key word to get back the employees
     */
    @Override
    public void run() {
        String msOut = information;
        OutputStream out;
        try {
            // Setting the socket
            setSocket();
            out = s.getOutputStream();

            // Writing and sending the message
            writeMessage(out, msOut);

            // Get prepared to receive the employees
            if(information.equals("Get Employees"))
                sync();

            // Closing the socket
            s.close();
        } catch (IOException e) {
            // If the communication failed
            System.out.println("Cannot connect with the server");
            hasCommunicate = false;
        }
    }

    /**
     * This function synchronize the employees
     */
    private void sync() {
        InputStream in;
        try {
            // Reading the list of employees
            in = s.getInputStream();
            information = readMessage(in);

            // Splitting employees by employees in an array of strings
            String[] message = information.split(";");

            // Clearing the previous employees
            employees.clear();

            // Adding all employees
            employees.addAll(Arrays.asList(message));

            // Closing the inputStream
            in.close();
        } catch (IOException e) {
            System.out.println("Cannot connect with the server");
        }
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<String> getEmployees() {
        return employees;
    }

    public boolean isHasCommunicate() {
        return hasCommunicate;
    }
}
