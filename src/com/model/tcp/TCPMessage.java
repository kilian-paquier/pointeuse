package com.model.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;


public class TCPMessage {
    protected byte[] buffer;
    ServerSocket ss;
    Socket s;
    InetSocketAddress isA;
    protected final int size = 8192;
    private int count;

    /** The set method for the buffer.
     * @param size the size of the buffer
     */
    void setStreamBuffer(int size) {
        if(size>0)
            buffer = new byte[size];
        else
            buffer = new byte[this.size];
    }

    /** The (simple) text write method.
     * @param out the OutputStream
     * @param msOut the message we're going to send
     * @throws IOException an exception
     */
    public void writeMessage(OutputStream out, String msOut) throws IOException {
        if((out!=null)&(msOut!=null)) {
            fillChar(msOut);
            out.write(buffer);
            out.flush();
            clearBuffer();
        }
    }

    /**
     * This function transforms a string message into a byte message in a buffer
     * @param msOut The message we're going to send
     */
    private void fillChar(String msOut) {
        if(msOut!=null)
            if(msOut.length() < buffer.length)
                for(int i=0;i<msOut.length();i++)
                    buffer[i] = (byte)msOut.charAt(i);
    }

    /**
     * This function clear the buffer
     */
    private void clearBuffer() {
        for(int i=0;i<buffer.length;i++)
            buffer[i] = 0;
    }

    /** The (simple) text read method.
     * @param in The InputStream
     * @return The message we have received
     * @throws IOException an exception
     */
    public String readMessage(InputStream in) throws IOException {
        if(in != null) {
            in.read(buffer);
            count = count();
            if(count>0)
                return new String(buffer,0,count);
        }
        return null;
    }

    /**
     * this function returns the length of the buffer
     * @return the length
     */
    protected int count() {
        for(int i=0;i<buffer.length;i++)
            if(buffer[i] == 0)
                return i;
        return buffer.length;
    }
}

