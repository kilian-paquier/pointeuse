package com.model.tcp;

import com.controller.CheckAppController;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TCPServerTracker extends TCPMessage implements Runnable {

    private String information = "noinfo";
    private List<String> employees = new ArrayList<>();
    private boolean running = true;

    public TCPServerTracker() {
        s = null;
        isA = null;
    }

    public String getInformation() {
        return information;
    }

    /**
     * Setting the socket
     * @throws IOException if the port or the inetAddress is already used
     */
    private void setSocket() throws IOException {
        isA = new InetSocketAddress("localhost", 8090);
        ss = new ServerSocket(isA.getPort());
        setStreamBuffer(ss.getReceiveBufferSize());
    }

    /**
     * This is the main function of the server, running on all time
     */
    @Override
    public void run() {
        InputStream in;
        try {
            setSocket();
            System.out.println("Tracker's socket set");
            // While the server is running, keep getting a communication
            while(running) {
                s = ss.accept(); // Waiting for a client
                in = s.getInputStream();
                information = readMessage(in); // Getting the message

                // Returning the list of employees if the message is "Get Employees"
                if (information.split(" ").length == 2) {
                    CheckAppController.settingsChanged(information.split(" ")[0], Integer.valueOf(information.split(" ")[1]));
                }
                // Else, it seems that's the employees' list
                else
                    sync();
                in.close();
                s.close();
            }
        } catch (IOException e) {
            System.out.println("Tracker's socket closed");
        }
        finally {
            try {
                if (s != null)
                    s.close();
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This function send all the employees as a string
     */
    private void sync() {
        // Splitting employees by employees in an array of strings
        String[] message = information.split(";");

        // Clearing the previous employees
        employees.clear();

        // Adding all employees
        employees.addAll(Arrays.asList(message));
        CheckAppController.setEmployees(employees);
    }

    /**
     * This function stop the server, when the main application is going to be off
     */
    public void close() {
        running = false;
        try {
            ss.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getEmployees() {
        return employees;
    }

    public void setEmployees(List<String> employees) {
        this.employees = employees;
    }
}
