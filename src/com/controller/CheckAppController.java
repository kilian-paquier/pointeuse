package com.controller;

import com.model.entities.TimeClock;
import com.model.tcp.TCPClient;
import com.model.tcp.TCPServerTracker;
import com.model.time.WorkToday;
import com.view.checkApp.CheckApp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class CheckAppController {
    private static List<String> employees = new ArrayList<>();
    private static String ip = "localhost";
    private static int port = 8085;
    private CheckApp checkApp;
    private static TCPServerTracker serverTracker;

    public CheckAppController(String name) {
       checkApp = new CheckApp(name);
       try {
           PrintWriter lock = new PrintWriter(new File(".\\data\\serverTracker.lock"));
           lock.write("");
           lock.close();
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       }
       serverTracker = new TCPServerTracker();
       new Thread(serverTracker).start();

       try {
           importCSV(".\\data\\employeesTracker.csv");
           readConf();
       } catch (IOException e) {
           e.printStackTrace();
       }
       sync();
       syncOffline();

        // Init the box with the employees, the label with the current date and the labels with the theory's hour and the real hour
        initEmployeeBox(checkApp.getEmployeesBox());
        initDay(checkApp.getDateLabel());
        initTime(checkApp.getTheoryHourLabel(), checkApp.getRealHourLabel());

        // Create a timer to update the hour every second
        Timer timer = new Timer(10000, e -> {
            initDay(checkApp.getDateLabel());
            initTime(checkApp.getTheoryHourLabel(), checkApp.getRealHourLabel());
            if ((checkApp.getEmployeesBox().getItemCount() - 1) != employees.size())
                initEmployeeBox(checkApp.getEmployeesBox());
        });
        timer.start();

        // Add the listener for a check
        checkApp.getCheckInOutButton().addActionListener(e -> {
            check(checkApp.getEmployeesBox());
            initEmployeeBox(checkApp.getEmployeesBox());
        });

        // Add the listener to update the list of employees
        checkApp.getSyncFromMainAppButton().addActionListener(e -> {
            sync();
            initEmployeeBox(checkApp.getEmployeesBox());
        });

        checkApp.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                checkApp.dispose();
                new File(".\\data\\serverTracker.lock").delete();
                serverTracker.close();
                try {
                    exportCSV(".\\data\\employeesTracker.csv");
                    saveConf();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    /**
     * This function save the configuration into a file
     */
    private void saveConf() {
        Properties properties = new Properties();
        properties.setProperty("IP", ip);
        properties.setProperty("Port", String.valueOf(port));
        try {
            properties.storeToXML(new FileOutputStream(".\\data\\clientTracker.xml"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function read the configuration from a file
     */
    private void readConf() {
        Properties properties = new Properties();
        try {
            properties.loadFromXML(new FileInputStream(".\\data\\clientTracker.xml"));
            ip = properties.getProperty("IP");
            port = Integer.valueOf(properties.getProperty("Port"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public static void setEmployees(List<String> employees) {
        CheckAppController.employees = employees;
    }

    /**
     * This function init the current date in the label which show it
     * @param dateLabel the label which show the current date
     */
    private void initDay(JLabel dateLabel) {
        DayOfWeek day = LocalDate.now().getDayOfWeek(); // Getting the current day of week
        int number = LocalDate.now().getDayOfMonth(); // getting the day of month
        int year = LocalDate.now().getYear(); // Getting the Year
        Month month = LocalDate.now().getMonth(); // getting the month
        String time = String.valueOf(day+" "+number+" "+month+" "+year); // Creating the string to show on the IHM
        dateLabel.setText(time); // Setting the string

        // Changing the font
        Font font = new Font("Arial",Font.BOLD,20);
        dateLabel.setFont(font);
    }

    /**
     * This function init two label which show the current time and the current time rounded
     * @param theoryHourLabel showing the current hour
     * @param realHourLabel showing the hour rounded
     */
    private void initTime(JLabel theoryHourLabel, JLabel realHourLabel) {
        // Getting the current hour without rounding
        LocalTime now = LocalTime.now().withSecond(0).withNano(0);
        theoryHourLabel.setText(String.valueOf(now));

        // Setting the font
        Font font = new Font("Arial",Font.BOLD, 20);
        theoryHourLabel.setFont(font);

        // Getting the rounding time and setting the font
        WorkToday nowRounded = new WorkToday(now);
        realHourLabel.setText("Let's say : "+String.valueOf(nowRounded.getRealHourMorning()));
        realHourLabel.setFont(font);
    }

    /**
     * This function init the box with all the employees
     * @param employeeBox the box which contains all the first name and last name of the employees
     */
    private void initEmployeeBox(JComboBox<Object> employeeBox) {
        // Removing all item from the box and adding the first item ""
        employeeBox.removeAllItems();
        employeeBox.addItem("");

        // Setting all items
        for (String employee : employees){
            employeeBox.addItem(employee.split(",")[1]+" "+employee.split(",")[2]);
        }
    }

    /**
     * This function is used to check and also send a TCP message with the information of the check
     * @param employeeBox the box which contains all the employees
     */
    public void check(JComboBox employeeBox) {
        // Getting the first and last name of the employee
        String[] employee = String.valueOf(employeeBox.getSelectedItem()).split(" ");

        // Verifying if the user chose an employee
        if (String.valueOf(employeeBox.getSelectedItem()).equals(""))
            JOptionPane.showMessageDialog(null,"You didn't choose an employee");
        else {
            // Searching the employee
            for (String  employee1 : employees) {
                // Found
                if (employee1.split(",")[1].equals(employee[0]) && employee1.split(",")[2].equals(employee[1])) {
                    // Creating a check point
                    TimeClock timeClock = new TimeClock(Integer.valueOf(employee1.split(",")[0]), employee1.split(",")[1], employee1.split(",")[2]);
                    String message = timeClock.toString(); // Setting the message to send into the TCP Client
                    TCPClient client = new TCPClient(ip, port); // Creating a client
                    client.setInformation(message); // Setting the information
                    client.run(); // Running the client (sending the message)

                    // Verifying if the communication succeeded
                    if (!client.isHasCommunicate()) {
                        try {
                            timeClock.saveToJson(); // Saving the check point if the communication didn't succeed
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * This function synchronize the list of employees at the lunching of the application
     */
    private void sync() {
        // Creating a client
        TCPClient client = new TCPClient(ip, port);
        client.setInformation("Get Employees"); // Setting the information to get the employees
        client.run(); // Running the client

        // Verifying if the communication succeed
        if (client.isHasCommunicate()) {
            List<String> employeesString = client.getEmployees();
            employees.clear();
            employees.addAll(employeesString);
        }
    }

    /**
     * This function synchronize the check did offline
     */
    private void syncOffline() {
        // Trying to send the past offline's checks
        List<TimeClock> timeClockList;
        try {
            // Reading all the past checks
            timeClockList = TimeClock.readFromJson();

            // For each, sending the check as one client
            for (TimeClock timeClock : timeClockList) {
                TCPClient client = new TCPClient(ip, port);
                client.setInformation(timeClock.toString());
                client.run();

                // Verifying if the communication succeed
                if (!client.isHasCommunicate()) {
                    timeClock.saveToJson();
                }
            }
        } catch (IOException e) {
            System.out.println("No offline checked to send");
        }
    }

    /**
     * This function permit to export to a csv file the list of the employees
     * @param fileName the filename (need to have all the way to the filename)
     * @throws IOException Thrown if there's an error in writing the document
     */
    private void exportCSV(String fileName) throws IOException {
        FileOutputStream file = new FileOutputStream(fileName); // Creating the file where we are going to write our information
        PrintWriter object = new PrintWriter(file); // Creating the printer

        // Saving the employees in many and many columns because there's many information
        for (String employee : employees) {
            object.write(employee.split(",")[0]); // Saving his first name
            object.write(',');
            object.write(employee.split(",")[1]); // Saving his last name
            object.write(',');
            object.write(employee.split(",")[2]); // Saving his id id
            object.write("\n");
        }

        object.close();
        file.close();
    }

    /**
     * This function permit to export to a csv file the list of the employees
     * @param fileName the filename (need to have all the way to the filename)
     * @throws IOException Thrown if there's an error in writing the document
     */
    private void importCSV(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;
        while ((line = reader.readLine()) != null) {
            employees.add(line);
        }
        serverTracker.setEmployees(employees);
    }

    public CheckApp getCheckApp() {
        return checkApp;
    }

    public static void settingsChanged(String ip, int port) {
        CheckAppController.ip = ip;
        CheckAppController.port = port;
    }
}


