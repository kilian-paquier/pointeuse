Les codes sources sont ensembles, au niveau des packages, il y a model, controller, view. Les vues ont �t� cr�es � l'aide de fichiers form.
Ces fichiers prennent en charge le placement sur la frame et la cr�ation des attributs. L'application centrale utilise toutes les classes
sauf TimeClock, CheckAppController, CheckView et TCPServerTracker.
TimeClock est un objet cr�� � chaque check r�alis� par un employ�.

Il est pr�f�rable de lancer l'application centrale puis la pointeuse, cependant on peut aussi faire l'inverse.

Voici les messages pouvant appara�tre dans la console :
"Cannot connect with the server" veut dire qu'un pointage n'a pas pu �tre envoy�, ou que l'application centrale n'a pas pu envoy� les employ�s
� sa fermeture.
"Socket set", "Tracker's socket set"
"Socket closed", "Tracker's socket closed"
"No offline checked to send" veut dire qu'il n'y a eu aucun pointage hors lignes enregistr�s � envoyer au d�marrage de la pointeuse

Un warning est cr�� lors de l'enregistrement d'un premier pointage hors ligne, je n'ai pas trouv� comment le retirer car cela vient de Gson.
Un dossier data est cr�� lors de l'utilisation des jar, s'il n'existe pas, il est dans le m�me dossier que les jar, il est donc pr�f�rable
de cr�er un dossier avec les deux jar.
Lors de la fermeture de la main app, l'IP et le port sont envoy�s � la pointeuse, dans le cas o� l'utilisateur aurait chang� les param�tres.
