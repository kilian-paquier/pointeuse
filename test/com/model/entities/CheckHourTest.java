package com.model.entities;

import com.model.time.TheoryCheckHours;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class CheckHourTest {
    private TheoryCheckHours checkHour;
    private TheoryCheckHours checkHourSet;

    @Before
    public void setUp() {
        checkHour = new TheoryCheckHours(15,15,15,15);

        LocalTime setInHour = LocalTime.of(18,18);
        LocalTime setOutHour = LocalTime.of(16,16);
        checkHourSet = new TheoryCheckHours();
        checkHourSet.setInHour(setInHour);
        checkHourSet.setOutHour(setOutHour);
    }

    @Test
    public void testCreateObjectCheckHour(){
        try {
            assertEquals(LocalTime.of(15,15), checkHour.getInHour());
            assertEquals(LocalTime.of(15,15), checkHour.getOutHour());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testSetInAndOut() {
        try {
            assertEquals(LocalTime.of(18, 15), checkHourSet.getInHour());
            assertEquals(LocalTime.of(16, 15), checkHourSet.getOutHour());
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
