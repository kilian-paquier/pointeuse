package com.model.entities;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompanyTest {
    private Company company = new Company();

    @Before
    public void setUp() {
        company = company.stubMethod();
    }

    @Test
    public void testAddEmployeesAndDelete() {
        try {
            Employee employeeToAdd = new Employee("dada","dede","nope");
            company.addEmployee(employeeToAdd);
            assertEquals(employeeToAdd, company.searchEmployee("dada","dede"));
            company.deleteEmployee("dada","dede");
            assertEquals(null, company.searchEmployee("dada","dede"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddDepartmentAndDelete() {
        try {
            Department departmentToAdd = new Department("computer science");
            company.addDepartments(departmentToAdd);
            assertEquals(departmentToAdd, company.searchDepartment("computer science"));
            company.deleteDepartment("computer science");
            assertEquals(null, company.searchDepartment("computer science"));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testSaveAndReadDatas() {
        try {
            company.saveDatas();
            Company newCompany = new Company();
            newCompany.readDatas();
            assertEquals(company.getNumberDepartments(), newCompany.getNumberDepartments());
            assertEquals(company.getNumberEmployees(), newCompany.getNumberEmployees());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testChangeAnEmployeeFromDepartment() {
        Employee employee = company.searchEmployee("employee0", "employee0");
        Department department1 = company.searchDepartment("department4");

        Department initialDepartment = company.searchDepartment("department0");

        company.changeDepartmentOfEmployee(department1, employee);
        assertEquals(employee, department1.searchEmployee("employee0", "employee0"));
        company.changeDepartmentOfEmployee(initialDepartment, employee);
        assertEquals(employee, initialDepartment.searchEmployee("employee0", "employee0"));
    }

    @Test
    public void testIsManagingForAll() {
        for (Employee employee : company.getManagersNotManagingWithEmployees()) {
            assertFalse(company.isManaging(employee));
        }
        for (Employee employee : company.getManagersNotManaging()) {
            assertFalse(company.isManaging(employee));
        }
        for (Manager manager : company.getManagersManaging()) {
            assertEquals(true,company.isManaging(manager));
        }
    }


}