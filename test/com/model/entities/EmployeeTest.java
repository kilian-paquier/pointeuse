package com.model.entities;

import com.model.time.TheoryCheckHours;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class EmployeeTest {
    private Employee employee;
    private Employee employee2;

    @Before
    public void setUp() {
        employee = new Employee(0, "Jean", "Michel", "jean.michel@hotmail.fr");
        for (DayOfWeek day : DayOfWeek.values()) {
            if (day != DayOfWeek.SUNDAY && day != DayOfWeek.SATURDAY) {
                employee.setTheoryCheckHours(day, LocalTime.of(9,0), LocalTime.of(18,15));
            }
        }

        employee2 = new Employee(0, "Jean", "Michel", "jean.michel@hotmail.fr");
        for (DayOfWeek day : DayOfWeek.values()) {
            if (day != DayOfWeek.SUNDAY && day != DayOfWeek.SATURDAY) {
                employee2.setTheoryCheckHours(day, LocalTime.of(9,0), LocalTime.of(18,15));
            }
        }
    }

    @Test
    public void testGetCheckHour(){
        try {
            TheoryCheckHours check = new TheoryCheckHours(9,0, 18,15);
            assertEquals(check.getInHour(), employee.getCheckHoursDay(LocalDate.now().getDayOfWeek()).getInHour());
            assertEquals(check.getOutHour(), employee.getCheckHoursDay(LocalDate.now().getDayOfWeek()).getOutHour());
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testCompareTimesAndAddCountHour() {
        try {
            LocalDate date = LocalDate.of(2018,4,30);
            employee.setWorkDaysCheck(LocalTime.of(10,15), date);
            employee.addCountHour(employee.compareTimeMorning(date));
            assertEquals(75,employee.getCountHour());

            employee2.setWorkDaysCheck(LocalTime.of(8,15), date);
            employee2.addCountHour(employee2.compareTimeMorning(date));
            assertEquals(-45, employee2.getCountHour());


            employee.addAfternoonTimeWorkToday(LocalTime.of(19,45));
            employee.addCountHour(employee.compareTimeAfternoon(date));
            assertEquals(-15,employee.getCountHour());

            employee2.addAfternoonTimeWorkToday(LocalTime.of(17,45));
            employee2.addCountHour(employee2.compareTimeAfternoon(date));
            assertEquals(-15,employee2.getCountHour());
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}