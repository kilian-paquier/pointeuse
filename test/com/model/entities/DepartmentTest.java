package com.model.entities;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepartmentTest {
    private Department department;

    @Before
    public void setUp() {
        department = new Department("computer science");
        for (int i = 0; i < 10; i++){
            Employee employee = new Employee("employee"+i, "employee"+1, "employee"+1);
            department.addEmployees(employee);
        }
    }

    @Test
    public void deleteEmployee() {
        try {
            department.deleteEmployee("employee5", "employee5");
            assertEquals(null, department.searchEmployee("employee5", "employee5"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteAll() {
        try {
            department.deleteAll();
            assertEquals(null, department.getManager());
            assertEquals(null, department.getEmployees());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}